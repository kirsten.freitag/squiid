# 1.0.6
 - Update Windows installer to add desktop shortcut by default for winget
 - Windows installer adds the binary to the PATH variable
 - Patches for flatpak
# 1.0.5
 - Patch release for flatpak

# 1.0.4
 - Patch release for flatpak

# 1.0.3
 - Patch release for flatpak

# 1.0.2
 - Patch release for flatpak

# 1.0.1
 - Add command line argument parsing

# 1.0.0
Initial release. Includes a functioning TUI frontend and engine/parser components