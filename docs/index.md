![Squiid Logo](https://gitlab.com/ImaginaryInfinity/squiid-calculator/squiid/-/raw/trunk/branding/squiidtext_documentation_outline.svg)

Squiid is a modular calculator written in Rust. It is currently early in development but is intended to replace our previous calculator, ImaginaryInfinity Calculator. The official documentation can be found hosted at [imaginaryinfinity.net/docs/squiid](https://imaginaryinfinity.net/docs/squiid).

## Features
- Simple terminal user interface using Ratatui
- Supports both RPN and algebraic input
- Plugin support will be added in the future

Click a link in the sidebar to access documentation