# Clear (Algebraic)
`clear`

This is the documentation for the algebraic mode clear command. You can find the RPN clear command documentation [here](/user/commands/rpn/clear)

The `clear` command will clear the history.
