# Less Than or Equal To
`elt`

The `elt` command will test if the left number is less than or equal to the right number. Returns 1 if true, 0 if false.

----

### Function Arguments
```plaintext
elt(left_operand, right_operand)
```

This is equivalent to `left_operand <= right_operand`

----

### Algebraic Example
```plaintext
elt(3, 4)
```

### RPN Example
```plaintext
3
4
elt
```