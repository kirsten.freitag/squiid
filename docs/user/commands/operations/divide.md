# Divide
`divide` / `/` 

The divide command will divide two numbers

----

### Algebraic Example
```plaintext
2/4
```

### RPN Example
```plaintext
2
4/
```