# Change Sign (RPN Only)
`chs` / `_`

The `chs` command will invert the sign of a number. This command is rebindable

----

### RPN Example
#### Input Stack:
```plaintext
2
chs
```
OR
```plaintext
2_
```
#### Result Stack:
```plaintext
-2
```