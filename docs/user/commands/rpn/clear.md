# Clear (RPN)
`clear`

This is the documentation for the RPN mode clear command. You can find the algebraic clear command documentation [here](/user/commands/functions/clear)

The `clear` command will clear the stack.

----

### RPN Example
#### Input Stack:
```plaintext
1
2
3
4
clear
```

#### Result Stack:
```plaintext

```