# Undo (RPN Only)
`undo`

The `undo` command will restore the stack to the state it was in before the most recent operation.
